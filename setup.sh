#!/bin/bash

PROJ_DIR=$(pwd)

# CLONE SUTTAS
echo "[+] Fetchning the MN suttas"

mkdir -p data/
cd data/

git clone -n --depth=1 --filter=tree:0 -b published https://github.com/suttacentral/bilara-data/
cd bilara-data
git sparse-checkout set --no-cone translation/en/sujato/sutta/mn
git checkout

cd $PROJ_DIR
echo "[+] Suttas fetched!"

# Parse suttas
echo "[+] Parsing suttas json files"

JSONS_PATH="data/bilara-data/translation/en/sujato/sutta/mn"

mkdir -p data/parsed-data/

function parse() {
  RESULT="data/parsed-data/$(basename $1 | sed 's/.json/.txt/')"
  grep -E '[a-z]' $1 | sed 's/^\s*".*": "//; s/\s*",$//' | grep -v '^$' > $RESULT
}

for json in $(ls ${JSONS_PATH}/*.json); do
  echo " - Parsing $(basename $json)"
  parse $json
done

# PIP
echo "[+] Installing pip requirements"
pip install -r requirements.txt
CMAKE_ARGS="-DLLAMA_CUBLAS=on" FORCE_CMAKE=1 pip install --upgrade --force-reinstall llama-cpp-python --no-cache-dir

# llama model
echo "[+] Downloading the llama-2-7b-chat.ggmlv3.q4_1 model"
#wget 'https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGUF/resolve/main/llama-2-7b-chat.Q4_K_M.gguf?download=true' -O ./data/llama-2-7b-chat.Q4_K_M.gguf
wget 'https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGML/resolve/main/llama-2-7b-chat.ggmlv3.q4_1.bin?download=true' -O ./data/llama-2-7b-chat.ggmlv3.q4_1.bin

# Init DG
echo "[+] Initializing the database"
python app.py

# Done
echo "[+] All done! Now run "
