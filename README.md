# Sutta Chat

## vast.ai setup

I use the `nvidia/cuda:12.0.1-devel-ubuntu22.04` img. Then I run:

```bash
apt install python3-venv python-is-python3 python3-wheel -y
git clone https://gitlab.com/forkfed/suttachat/ -b main
cd suttachat
python -m venv venv
source venv/bin/activate
./setup.sh
```
