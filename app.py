from langchain_community.document_loaders import TextLoader, DirectoryLoader
from langchain_core.prompts import PromptTemplate
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import FAISS
from langchain_community.llms import CTransformers
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain.chains import RetrievalQA
from pathlib import Path
import chainlit as cl

FAISS_DB_PATH = './data/db_faiss'
DATA_PATH = 'data/parsed-data/'


def create_prompt_template():
    prompt_template_txt = Path('prompt.txt').read_text()
    return PromptTemplate(template=prompt_template_txt,
                          input_variables=['context', 'question'])


def load_llm():
    llm = CTransformers(
        # model downloaded from https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGML
        model="data/llama-2-7b-chat.ggmlv3.q4_1.bin",
        model_type="llama",
        max_new_tokens=512,
        temperature=0.5,
        config={
            'gpu_layers': 100,
            'context_length': 5000,
        }
    )
    return llm


def qa_bot():
    embeddings = HuggingFaceEmbeddings(model_name="sentence-transformers/all-MiniLM-L6-v2",
                                       model_kwargs={'device': 'cuda'})
    db = FAISS.load_local(FAISS_DB_PATH, embeddings,
                          allow_dangerous_deserialization=True)
    llm = load_llm()
    prompt = create_prompt_template()
    retriever = db.as_retriever(search_kwargs={'k': 2})
    qa = RetrievalQA.from_chain_type(llm=llm,
                                     chain_type='stuff',
                                     retriever=retriever,
                                     return_source_documents=True,
                                     chain_type_kwargs={'prompt': prompt})
    return qa


def create_vector_db():
    loader = DirectoryLoader(DATA_PATH,
                             glob='*.txt',
                             loader_cls=TextLoader)
    documents = loader.load()
    text_splitter = RecursiveCharacterTextSplitter(
        chunk_size=500, chunk_overlap=50)
    texts = text_splitter.split_documents(documents)
    embeddings = HuggingFaceEmbeddings(model_name='sentence-transformers/all-MiniLM-L6-v2',
                                       model_kwargs={'device': 'cuda'})
    db = FAISS.from_documents(texts, embeddings)
    db.save_local(FAISS_DB_PATH)


@cl.on_chat_start
async def start():
    chain = qa_bot()
    msg = cl.Message(content="Starting your SuttAI Bot!...")
    await msg.send()
    msg.content = "Welcome to SuttAI Bot!. Ask your question here:"
    await msg.update()
    cl.user_session.set("chain", chain)


@cl.on_message
async def main(message):
    chain = cl.user_session.get("chain")
    cb = cl.AsyncLangchainCallbackHandler(
        stream_final_answer=True, answer_prefix_tokens=["FINAL", "ANSWER"]
    )
    cb.answer_reached = True

    res = await chain.acall(message.content, callbacks=[cb])
    answer = res["result"]
    sources = res["source_documents"]

    if sources:
        answer += f"\nSources:" + str(sources)
    else:
        answer += "\nNo sources found"

    await cl.Message(content=answer).send()


if __name__ == "__main__":
    create_vector_db()
